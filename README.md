# Alex's Notes

## Task 1

* Created "sponge" repository on my [BitBucket](https://bitbucket.org/tissandier/sponge/) account 
* Cloned with Sourcetree to my local machine
* Copied contents of "js-developer-test.zip"
* Made initial commit and pushed to BitBucket
* Opened in Visual Studio Code

## Task 2

Time taken: ~10 mins (I got distracted looking through the rest of the project).

## Task 3

Straightforward but I didn't realise you needed {{{three brackets}}} to render HTML in Handlebars. I figured it out by looking at the other template tags on the page and then read a stackoverflow answer that explained it.

Time taken: ~10 mins

## Task 4

* Changed "CSS, & JavaScript" to "CSS and JavaScript"
* Fixed formatting: "pages, user"
* Fixed typos: "applications", "mobile", "steps"
* Turned the XHMTL compatibility steps into an array (probably overkill!)
* Added a new JSON string for the text after the list (I find it hard to come up with good variable names...)
* Fixed typos: "markdown", "content", "finalized" to "finalised" (assuming this is a U.K. website), "3014" to "2014"
* Fixed formatting errors (extra spaces etc.)

Time taken: 25 mins

## Task 5

* Using flexbox for the tab headings for conveniece, but flexbox is not supported before IE10
* Tab headings maybe a bit too squashed together on mobile?

Time taken: 60 mins

## Task 6

* Moving my About tabs into a handlebars template broke the jQuery witch switches between tabs. Moving all the jQuery inside the "onReady" function of resContent fixed it (because now it waits until Handlebars has populated the DOM).
* Added a Handlebars toLowerCase helper function so tab titles can be used as class names (found on StackOverflow).
* Currently have to loop through tabs twice because of how I structured the HTML in Task Five, could probably rewrite it or do something with flexbox to just loop once...

Time taken: 50 mins

## Task 7

* Should the function take three separate parameters for template, JSON, and destination, or just one for all three? Three is more flexible.
* Should this be three separate functions?

Time taken: 80 mins (took a while to understand content.js)

## Tasks 8 and 10

* Set up gulp tasks to compile SASS and watch for changes
* Created separate _variables for SASS colours

Time taken: 30 mins

## Task 9

* IE9 requirement means I can't use flexbox.
* Is calc allowed? caniuse.com says it works on IE9.
* Can't figure out how to get the "Wrapper" text to poke out of the bottom of #boxes!

Time taken: 70 mins

## Task 11

* Had to google around for the class / constructor syntax. I thought I almost had it working but the JSON data won't load (there are no error messages...)
* Although I'm familiar with using classes from C#, I don't know enough about how they work in ES6 to figure this out without more research (especially how local variables work within the class)
* Gave up and reverted to previous version (I put my attempt in /app/js/ES6Attempt)

Time taken: 40 mins

## Task 12

* Added a media query that increases font size on body element by 125%. Seems to work but I feel like I might be missing something?

Time taken: 10 mins

## Task 13

* Followed the docs for gulp-babel
* Installed new node packages:
    "gulp-babel": "latest",
    "@babel/core": "^7.4.3",
    "@babel/preset-env": "^7.4.3"
* Added new gulp task "transpile" which compiles using Babel's "preset-env" preset (which compiles to ES5)

Time taken: 15 mins

# Sponge UK Developer Test

## Getting started

In order to run this test, you must either host this project on a web server or enable local file execution in your browser.

### NodeJS server (preferred)

1. `npm i`
2. `npm start`
3. [Open the test in your browser](http://localhost:3000)

### Local web server

* [WAMP](http://www.wampserver.com/en/) - Windows, Apache, MySQL, PHP
* [MAMP](https://www.mamp.info/en/) - Mac, Apache, MySQL, PHP
* [Fenix](http://fenixwebserver.com/)
* [LightTPD](http://www.lighttpd.net/)

### Chrome with local file support

If you aren't able to install or run a web server, you can open the files directly in Chrome. To do this, you must [enable the requests with the protocol of `file://`](https://github.com/mrdoob/three.js/wiki/How-to-run-things-locally).

## Tools required

* An IDE of your choice (e.g. [Sublime Text](https://www.sublimetext.com/3))
* A web browser (e.g. [Chrome](https://www.google.com/chrome/))
* A copy of Microsoft Word (for opening the example text - you may be able to use [Google Docs](https://www.google.co.uk/docs/about/) instead)