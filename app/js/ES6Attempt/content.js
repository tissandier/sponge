/*
 =====================================================

   _____                                _    _ _  __
  / ____|                              | |  | | |/ /
 | (___  _ __   ___  _ __   __ _  ___  | |  | | ' /
  \___ \| '_ \ / _ \| '_ \ / _` |/ _ \ | |  | |  <
  ____) | |_) | (_) | | | | (_| |  __/ | |__| | . \
 |_____/| .__/ \___/|_| |_|\__, |\___|  \____/|_|\_\
        | |                 __/ |
        |_|                |___/

 =====================================================
 SPONGE UK DEVELOPER TEST
 JSON parser and event handler
 ES6 Class Version
 =====================================================
*/

class ContentInstance {

	constructor(strDataLocation) {
		this.objContent = {};
		this.arrOnReady = [];
		this.blReady = false;
		$.getJSON(strDataLocation,
			function (objResponse) {
				this.objContent = objResponse;
				this.blReady = true;

				/**
				 * Execute all the ready functions once loaded
				 */
				$.each(this.arrOnReady,
					function (intIndex, funDoOnReady) {
						funDoOnReady.call();
					}
				);
			}
		);

	}

	/**
	 * Register a method to execute once loaded
	 */
	onReady(funDoOnReady) {
		if (this.blReady) {
			funDoOnReady.call();
		} else {
			this.arrOnReady.push(funDoOnReady);
		}
	}

	getItem(intItem) {
		return objContent[intItem];
	}

	populateContent(template, content, destination) {

		var strHeaderSource = $(template).html();
		try {
			var resHeaderTemplate = Handlebars.compile(strHeaderSource);
		} catch (err) {
			console.error("Couldn't find a template called: " + template);
			return;
		}
		var JSON = this.getItem(content);
		if (JSON != undefined) {
			var strHeaderHTML = resHeaderTemplate(JSON);
		} else {
			console.error("Couldn't find any JSON content called: " + content);
		}
		if ($(destination).length) {
			$(destination).html(strHeaderHTML);
		} else {
			console.error("Destination element not found in DOM: " + destination)
		}

	}

}
export default ContentInstance;

/*
      ,'``.._   ,'``.
     :,--._:)\,:,._,.:       All Glory to
     :`--,''   :`...';\      the HYPNOTOAD!
      `,'       `---'  `.
      /                 :
     /                   \
   ,'                     :\.___,-.
  `...,---'``````-..._    |:       \
    (                 )   ;:    )   \  _,-.
     `.              (   //          `'    \
      :               `.//  )      )     , ;
    ,-|`.            _,'/       )    ) ,' ,'
   (  :`.`-..____..=:.-':     .     _,' ,'
    `,'\ ``--....-)='    `._,  \  ,') _ '``._
 _.-/ _ `.       (_)      /     )' ; / \ \`-.'
`--(   `-:`.     `' ___..'  _,-'   |/   `.)
    `-. `.`.``-----``--,  .'
      |/`.\`'        ,',');
          `         (/  (/
 */
