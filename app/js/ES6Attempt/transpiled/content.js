"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/*
 =====================================================

   _____                                _    _ _  __
  / ____|                              | |  | | |/ /
 | (___  _ __   ___  _ __   __ _  ___  | |  | | ' /
  \___ \| '_ \ / _ \| '_ \ / _` |/ _ \ | |  | |  <
  ____) | |_) | (_) | | | | (_| |  __/ | |__| | . \
 |_____/| .__/ \___/|_| |_|\__, |\___|  \____/|_|\_\
        | |                 __/ |
        |_|                |___/

 =====================================================
 SPONGE UK DEVELOPER TEST
 JSON parser and event handler
 ES6 Class Version
 =====================================================
*/
var ContentInstance =
/*#__PURE__*/
function () {
  function ContentInstance(strDataLocation) {
    _classCallCheck(this, ContentInstance);

    this.objContent = {};
    this.arrOnReady = [];
    this.blReady = false;
    $.getJSON(strDataLocation, function (objResponse) {
      this.objContent = objResponse;
      this.blReady = true;
      /**
       * Execute all the ready functions once loaded
       */

      $.each(this.arrOnReady, function (intIndex, funDoOnReady) {
        funDoOnReady.call();
      });
    });
  }
  /**
   * Register a method to execute once loaded
   */


  _createClass(ContentInstance, [{
    key: "onReady",
    value: function onReady(funDoOnReady) {
      if (this.blReady) {
        funDoOnReady.call();
      } else {
        this.arrOnReady.push(funDoOnReady);
      }
    }
  }, {
    key: "getItem",
    value: function getItem(intItem) {
      return objContent[intItem];
    }
  }, {
    key: "populateContent",
    value: function populateContent(template, content, destination) {
      var strHeaderSource = $(template).html();

      try {
        var resHeaderTemplate = Handlebars.compile(strHeaderSource);
      } catch (err) {
        console.error("Couldn't find a template called: " + template);
        return;
      }

      var JSON = this.getItem(content);

      if (JSON != undefined) {
        var strHeaderHTML = resHeaderTemplate(JSON);
      } else {
        console.error("Couldn't find any JSON content called: " + content);
      }

      if ($(destination).length) {
        $(destination).html(strHeaderHTML);
      } else {
        console.error("Destination element not found in DOM: " + destination);
      }
    }
  }]);

  return ContentInstance;
}();

var _default = ContentInstance;
/*
      ,'``.._   ,'``.
     :,--._:)\,:,._,.:       All Glory to
     :`--,''   :`...';\      the HYPNOTOAD!
      `,'       `---'  `.
      /                 :
     /                   \
   ,'                     :\.___,-.
  `...,---'``````-..._    |:       \
    (                 )   ;:    )   \  _,-.
     `.              (   //          `'    \
      :               `.//  )      )     , ;
    ,-|`.            _,'/       )    ) ,' ,'
   (  :`.`-..____..=:.-':     .     _,' ,'
    `,'\ ``--....-)='    `._,  \  ,') _ '``._
 _.-/ _ `.       (_)      /     )' ; / \ \`-.'
`--(   `-:`.     `' ___..'  _,-'   |/   `.)
    `-. `.`.``-----``--,  .'
      |/`.\`'        ,',');
          `         (/  (/
 */

exports["default"] = _default;