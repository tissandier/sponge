/*
 =====================================================

   _____                                _    _ _  __
  / ____|                              | |  | | |/ /
 | (___  _ __   ___  _ __   __ _  ___  | |  | | ' /
  \___ \| '_ \ / _ \| '_ \ / _` |/ _ \ | |  | |  <
  ____) | |_) | (_) | | | | (_| |  __/ | |__| | . \
 |_____/| .__/ \___/|_| |_|\__, |\___|  \____/|_|\_\
        | |                 __/ |
        |_|                |___/

=====================================================
 SPONGE UK DEVELOPER TEST
 Page-specific JS
=====================================================
*/

jQuery(
	function ($) {
		/**
		 * A new instance of the content parser using the content JSON file
		 */
		var resContent = new Content('app/data/content.json');

		/**
		 * Register a Handlebars helper for the difficulty stars
		 */
		Handlebars.registerHelper('difficulty',
			function (intStars) {
				var strHTMLStarsOut = '';

				for (var intStar = 0; intStar < intStars; intStar++) {
					strHTMLStarsOut += '<i class="fa fa-star"></i>';
				}

				for (var intBlankStar = intStars; intBlankStar < 5; intBlankStar++) {
					strHTMLStarsOut += '<i class="fa fa-star-o"></i>';
				}

				return strHTMLStarsOut;
			}
		);

		/**
		 * Register a Handlebars helper to turn Tab titles into lowercase for class names 
		 */
		Handlebars.registerHelper('toLowerCase', function (str) {
			return str.toLowerCase();
		});

		/**
		 * When the content file is ready, actually populate the content
		 */
		resContent.onReady(
			function () {
				resContent.populateContent('#header-template', 'header', '#header');
				resContent.populateContent('#task-template', 'tasks', '#tasks');
				resContent.populateContent('#content-template', 'content', '#content');
				resContent.populateContent('#documentation-template', 'docs', '#documentation');
				resContent.populateContent('#about-template', 'about', '#about');

				/**
				 * About Me tabs behaviour
				 */
				$('.about-heading:first').addClass('active-heading'); // Default to first tab
				$('.about-content:first').addClass('active-content'); 
				$('.about-heading').click(function (e) {
					e.preventDefault(); // Stops the hash appearing in address bar
					$('.active-heading').removeClass('active-heading'); // Remove currently active heading
					$('.active-content').removeClass('active-content'); // Hide whichever tab is currently visible
					$(this).addClass('active-heading');
					$($(this).data('target')).addClass('active-content'); // Get tab ID from anchor data element and set it to active
				})
			}
		);

	}
);