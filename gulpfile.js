const gulp = require("gulp");
const sass = require("gulp-sass");
const babel = require("gulp-babel");

function sassCompile() {
    return gulp
        .src("scss/spongetest.scss")
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(gulp.dest("app/css"));
}

function sassWatch() {
    return gulp
    .watch([
        "scss/**/*.scss"
        ], 
        sassCompile
    );
}

function transpile(){
    return gulp
    .src("app/js/ES6Attempt/content.js")
    .pipe(babel({presets: ['@babel/env']}))
    .pipe(gulp.dest("app/js/ES6Attempt/transpiled"))
}

const watch = sassWatch;

exports.transpile = transpile;
exports.sass = sassCompile;
exports.watch = watch;
